from pdf2docx import Converter
import os
import time

# pdf2image模块需要依赖poppler运行
# pdf2image官网：https://pypi.org/project/pdf2image/
# poppler下载地址: https://github.com/oschwartz10612/poppler-windows/releases/
# windows模式下，下载完毕如下手动指定运行路径即可
# 安装模块：pip install pdf2docx

# 定义源PDF文件和目标docx文件的路径
# pdf_file = r'C:\\Users\\ABCD\\Desktop\\XYZ/Document1.pdf'
# docx_file = r'C:\\Users\\ABCD\\Desktop\\XYZ/sample.docx'

pdf_file = r'D:\\Python\\temp\\协定存款合同-1039.pdf'
docx_file = r'D:\\Python\\temp\\协定存款合同-1039.docx'

# 创建一个转换器对象
cv = Converter(pdf_file)

time.sleep(1)

# 转换PDF文件到docx文件
cv.convert(docx_file)

# 关闭转换器对象
cv.close()

# 遍历生成的文件
files = os.listdir(r'D:\\Python\\temp')
print(files)

