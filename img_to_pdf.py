# 导入所需的模块
from PIL import Image
import os

# 定义一个函数，将给定的图片文件夹中的所有图片转换为PDF，并保存到指定的输出文件夹中
def img_to_pdf(img_folder, output_folder):
    img_folder = 'D:\Python\img'
    output_folder = 'D:\Python\img'
    # 遍历图片文件夹中的所有文件
    for file in os.listdir(img_folder):
        # 获取文件的完整路径
        file_path = os.path.join(img_folder, file)
        # 判断文件是否是图片格式
        if file_path.lower().endswith((".png", ".jpg", ".jpeg", ".bmp")):
            # 用PIL模块打开图片
            img = Image.open(file_path)
            # 获取图片的文件名（不含扩展名）
            file_name = os.path.splitext(file)[0]
            # 生成PDF文件的完整路径
            pdf_path = os.path.join(output_folder, file_name + ".pdf")
            # 将图片转换为RGB模式（如果是其他模式，如CMYK）
            img = img.convert("RGB")
            # 将图片保存为PDF格式
            img.save(pdf_path, "PDF")
            # 关闭图片文件
            img.close()
            # 打印转换成功的信息
            print(f"{file} converted to {file_name}.pdf")

# 调用函数，将img_folder中的图片转换为PDF，并保存到output_folder中
img_to_pdf("img_folder", "output_folder")
