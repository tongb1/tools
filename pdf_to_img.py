from pdf2image import convert_from_path
import os
# pdf2image模块需要依赖poppler运行
# pdf2image官网：https://pypi.org/project/pdf2image/
# poppler下载地址: https://github.com/oschwartz10612/poppler-windows/releases/
# windows模式下，下载完毕如poppler_path手动指定运行路径即可
# 安装模块：pip install pdf2image

images = convert_from_path('D:\Python\img\华为云认证授权书.pdf', poppler_path=r'D:\Temp\poppler-23.05.0\Library\bin')

for i in range(len(images)):
    images[i].save('D:\Python\img\img' + str(i + 1) + '.jpeg', 'JPEG')

# 遍历生成的图片
files = os.listdir('D:\\Python\\img\\')
print(files)